# showstart-demo

#### 项目介绍
🪐演出发售购买点评社区

🏗基于Node.js，采用MVC架构，SSR的全栈项目

📦 Express + 🥭 MongoDB Atlas(Mongoose)+ 🐶 Pug

🌟值得关注的信息：json-web-token, bcrypt, axios, 💰alipay, 🌍高德地图API,Multer...

#### 目前实现的功能
1. 用户注册/登陆/退出登陆/注销账号/更新密码/更新邮箱或用户名/授权管理
2. 根据用户身份（user/admin）/状态（logged in or not）条件渲染前端页面
3. 全局错误处理，生产/开发环境不同的错误信息处理
4. 可以根据演出名过滤出相关演出信息
5. alipay沙箱环境购票

#### 预期实现的功能
1. 忘记密码用户通过邮箱验证重置密码（nodemailer)
2. 用户上传头像（Multer）
3. 管理员发布演出信息（包括上传演出照片）
4. 用户对演出发表评论，星级评价
5. 统计计算星级评价均分
6. 管理员管理用户和演出信息

#### 更多信息
🔗 课程主页：
[原版课程 | Udemy](https://www.udemy.com/course/nodejs-express-mongodb-bootcamp/)，
[国内搬运 | bilibili](https://www.bilibili.com/video/BV1FY4y1H7ka?spm_id_from=333.999.0.0)

📒 课程笔记：
[我的掘金专栏](https://juejin.cn/column/7091495734398156836)

#### 说明
仍在开发阶段，个人学习使用，欢迎留言交流。

