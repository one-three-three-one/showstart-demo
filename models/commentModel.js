const mongoose = require('mongoose');

// comment / rating / createdAt / ref to show / ref to user
const commentSchema = new mongoose.Schema({
  comment: {
    type: String,
    required: [true, 'Comment can not be empty']
  },
  rating: {
    type: Number,
    min: 1,
    max: 5,
    required: [true, 'rating can not be empty']
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  show: {
    type: mongoose.Schema.ObjectId,
    ref: 'Show',
    required: [true, 'Review must belong to a show.']
  },
  user: {
    type: mongoose.Schema.ObjectId,
    ref: 'User',
    required: [true, 'Review must belong to a user']
  }
},{
  toJSON:{virtuals:true},
  toObject:{virtuals:true}
});

commentSchema.index({ show: 1, user: 1 }, { unique: true });

/*优化tip：mongoose提供的虚拟填充 -virtual populate
可以避免show模型中评论id数组的增长，浪费内存空间*/
commentSchema.pre(/^find/, function(next) {
  this.populate({
    path: 'user',
    select: 'name'
  });
  // .populate({
  //   path: 'show',
  //   select: 'name'
  // })
  next();
});

// commentSchema.statics.calcAverageRatings = async function(showId) {
//   const stats = await this.aggregate([
//     {
//       $match: { show: showId }
//     },
//     {
//       $group: {
//         _id: '$show',
//         nRating: { $sum: 1 },
//         avgRating: { $avg: '$rating' }
//       }
//     }
//   ]);
//   // console.log(stats);

//   if (stats.length > 0) {
//     await Show.findByIdAndUpdate(showId, {
//       ratingsQuantity: stats[0].nRating,
//       ratingsAverage: stats[0].avgRating
//     });
//   } else {
//     await Show.findByIdAndUpdate(showId, {
//       ratingsQuantity: 0,
//       ratingsAverage: 4.5
//     });
//   }
// };

// commentSchema.post('save', function() {
//   // this points to current review
//   this.constructor.calcAverageRatings(this.show);
// });

// findByIdAndUpdate
// findByIdAndDelete
commentSchema.pre(/^findOneAnd/, async function(next) {
  this.r = await this.findOne();
  // console.log(this.r);
  next();
});

// commentSchema.post(/^findOneAnd/, async function() {
//   // await this.findOne(); does NOT work here, query has already executed
//   await this.r.constructor.calcAverageRatings(this.r.show);
// });


const Comment = mongoose.model('Comment', commentSchema);
module.exports = Comment;
