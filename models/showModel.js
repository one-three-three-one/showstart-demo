const mongoose = require('mongoose');
const slugify = require('slugify');

const showSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, 'A show must have a name'],
      unique: true,
      trim: true
    },
    slug: String,
    duration: {
      type: Number,
      required: [true, 'A show must have a duration']
    },
    ratingsAverage: {
      type: Number,
      default: 4.5,
      min: [1, 'Rating must be above 1.0'],
      max: [5, 'Rating must be below 5.0'],
      set: val => Math.round(val * 10) / 10 // 4.666666, 46.6666, 47, 4.7
    },
    ratingsQuantity: {
      type: Number,
      default: 0
    },
    price: {
      type: Number,
      required: [true, 'A show must have a price']
    },
    summary:{
      type:String
    },
    description: {
      type: String,
      trim: true
    },
    imageCover: {
      type: String,
      required: [true, 'A show must have a cover image']
    },
    images: [String],
    startDates: [Date],
    startLocation: {
      // GeoJSON
      type: {
        type: String,
        default: 'Point',
        enum: ['Point']
      },
      coordinates: [Number],
      address: String,
      description: String
    },
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true }
  }
);
// Virtual populate :
showSchema.virtual('durationWeeks').get(function (){
  return this.duration/7;
})
//&&&&& Virtual field -- "comments" does not exist in database &&&&&
showSchema.virtual('comments', {
  ref: 'Comment',
  foreignField: 'show', //ref (commentModel)中，当前model (showModel)被引用的名字
  localField: '_id'
});

// DOCUMENT MIDDLEWARE: runs before .save() and .create()
showSchema.pre('save', function(next) {
  this.slug = slugify(this.name, { lower: true });
  next();
});

// QUERY MIDDLEWARE
showSchema.pre(/^find/, function(next) {
  this.start = Date.now();
  next();
});

showSchema.post(/^find/, function(docs, next) {
  console.log(`Query took ${Date.now() - this.start} milliseconds!`);
  next();
});

const Show = mongoose.model('Show', showSchema);

module.exports = Show;
