// const catchAsync = require('./../utils/catchAsync');
const Comment = require('./../models/commentModel');
const simpleCurd = require('./simpleCurd');

exports.setShowUserIds = (req, res, next) => {
  // Allow nested routes
  if (!req.body.show) req.body.show = req.params.showId;
  if (!req.body.user) req.body.user = req.user.id;
  next();
};
exports.createComment = simpleCurd.createOne(Comment);

exports.getAllComments = simpleCurd.getAll(Comment);

exports.getComment = simpleCurd.getOne(Comment);

exports.updateComment = simpleCurd.updateOne(Comment);

exports.deleteComment = simpleCurd.deleteOne(Comment);
