const dotenv = require('dotenv');
//引入 alipay sdk 模块
const AliPaySdk = require('alipay-sdk').default;
//引入 alibapay form 模块
const AlipayForm = require('alipay-sdk/lib/form').default;
const Booking = require('../models/bookingModel');
const simpleCurd = require('./simpleCurd');

dotenv.config({ path: './../config.env' });
//初始化alipay 配置
const alipaySdk = new AliPaySdk({
  appId: process.env.ALIPAY_APPID, // 自己的id
  gateway: "https://openapi.alipaydev.com/gateway.do", // 支付宝官网沙箱测试网关
  privateKey: process.env.ALIPAY_PRIVATE_KEY, // 应用私钥
  alipayPublicKey: process.env.ALIPAY_PUBLIC_KEY // 支付宝公钥
});

exports.getBookingForm = async (req, res) => {
  // 实例化 AlipayForm
  const formData = new AlipayForm();

  // 下面是官网的测试代码
  formData.setMethod('get');
  formData.addField('returnUrl', 'http://localhost:3000'); //支付成功的回调
  formData.addField('bizContent', {
    outTradeNo: Math.random(), // 订单号
    productCode: 'FAST_INSTANT_TRADE_PAY', // 产品码
    totalAmount: '9.9', // 商品金额
    subject: '出售商品的标题', // 出售商品的标题
    body: '出售商品的内容' // 出售商品的内容
  });

  //执行结果
  const reult = await alipaySdk.exec(
    'alipay.trade.page.pay',
    {},
    { formData: formData }
  );

  res.status(200).send({
    code: reult
  });
};

// exports.createBooking = simpleCurd.createOne(Booking);
// exports.getBooking = simpleCurd.getOne(Booking);
// exports.getAllBookings = simpleCurd.getAll(Booking);
// exports.updateBooking = simpleCurd.updateOne(Booking);
// exports.deleteBooking = simpleCurd.deleteOne(Booking);
