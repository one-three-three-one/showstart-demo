const Show = require('../models/showModel');
const catchAsync = require('../utils/catchAsync');
const AppError = require('../utils/appError');

exports.getOverview = catchAsync(async (req, res, next) => {
  // console.log(req.query.filter);
  if (req.query.filter === '' || req.query.filter === undefined) {
    const shows = await Show.find();
    return res.status(200).render('overview', {
      title: '所有演出',
      shows
    });
  } else {
    const filter = new RegExp(`${req.query.filter}`, 'i');
    // console.log(filter);
    const shows = await Show.find({ name: { $regex: filter } });
    if (shows.length === 0) {
      return res.status(200).render('error', {
        title: '出错了！',
        msg: '没有找到对应的演出，请重新输入关键词！'
      });
    }
    res.status(200).render('overview', {
      title: '所有演出',
      shows
    });
  }
});

exports.getShow = catchAsync(async (req, res, next) => {
  const show = await Show.findOne({ slug: req.params.slug }).populate({
    path: 'comments',
    fields: 'user rating comment'
  });
  if (!show) {
    return next(new AppError('路径不正确', 404));
  }

  res.status(200).render('show', {
    title: `${show.name} `,
    show
  });
});

exports.getLoginForm = (req, res) => {
  res.status(200).render('login', {
    title: '登录到您的账户'
  });
};

exports.getSignupForm = (req, res) => {
  res.status(200).render('signup', {
    title: '注册新用户'
  });
};

exports.getAccount = (req, res) => {
  res.status(200).render('account', {
    title: '个人主页'
  });
};

// exports.getMyShows = catchAsync(async (req, res, next) => {
//   // 1) Find all bookings
//   const bookings = await Booking.find({ user: req.user.id });

//   // 2) Find shows with the returned IDs
//   const showIDs = bookings.map(el => el.show);
//   const shows = await Show.find({ _id: { $in: showIDs } });

//   res.status(200).render('overview', {
//     title: '我的预定',
//     shows
//   });
// });

exports.developing = (req, res) => {
  res.status(200).render('developing', {
    title: '正在开发中...'
  });
};
