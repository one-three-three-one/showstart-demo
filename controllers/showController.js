const Show = require('../models/showModel');
const simpleCurd = require('./simpleCurd');

exports.getAllShows = simpleCurd.getAll(Show);

exports.getShow = simpleCurd.getOne(Show, 'comments');

exports.createShow = simpleCurd.createOne(Show);

exports.updateShow = simpleCurd.updateOne(Show);

exports.deleteShow = simpleCurd.deleteOne(Show);
