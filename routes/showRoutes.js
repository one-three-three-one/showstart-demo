const express = require('express');
const showController = require('../controllers/showController');
const authController = require('../controllers/authController');
const router = express.Router();

// router.param('id', showController.checkID);

router
  .route('/')
  .get(showController.getAllShows)
  .post(
    authController.protect,
    authController.restrictTo('admin'),
    showController.createShow
  );

router
  .route('/:id')
  .get(showController.getShow)
  .patch(
    authController.protect,
    authController.restrictTo('admin'),
    showController.updateShow
  )
  .delete(
    authController.protect,
    authController.restrictTo('admin'),
    showController.deleteShow
  );
  
module.exports = router;
