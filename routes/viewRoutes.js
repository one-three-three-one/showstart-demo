const express = require('express');
const viewController = require('../controllers/viewController');
const authController = require('../controllers/authController');

const router = express.Router();

//预订成功界面
// router.use(viewController.alerts);

/*isLoggedIn need to be rendered before all pug templates ,but in
authController.protect ,we already access the current user info 
so router.use(authController.isLoggedIn); is no longer needed*/

router.get('/', authController.isLoggedIn, viewController.getOverview);
router.get('/show/:slug', authController.isLoggedIn, viewController.getShow);
router.get('/login', authController.isLoggedIn, viewController.getLoginForm);
router.get('/signup', authController.isLoggedIn, viewController.getSignupForm);

router.get('/me', authController.protect, viewController.getAccount);
// 我的预定
router.get('/my-shows', authController.protect, viewController.developing);
router.get('/my-comments', authController.protect, viewController.developing);

module.exports = router;
