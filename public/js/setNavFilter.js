const navSearchForm = document.querySelector('.nav__search');
const search = async filter => {
  try {
    const res = await axios({
      method: 'GET',
      url: `http://localhost:3000/?filter=${filter}`
    });
    if (res.status === 200) {
      location.assign(`/?filter=${filter}`)
    }
  } catch (err) {
    console.log(err);
  }
};
if (navSearchForm) {
  navSearchForm.addEventListener('submit', event => {
    event.preventDefault()
    const filter = document.querySelector('.nav__search-input').value;
    search(filter);
  });
}
