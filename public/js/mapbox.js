/* eslint-disable */
const mapContainer = document.getElementById('mapContainer');
window.onLoad = function() {
  if (mapContainer) {
    const lat = JSON.parse(mapContainer.dataset.location).coordinates[0];
    const lng = JSON.parse(mapContainer.dataset.location).coordinates[1];
    // console.log(JSON.parse(mapContainer.dataset.location))
    const map = new AMap.Map('mapContainer', {
      zoom: 12,
      center: [-lat, lng],
      viewMode: '3D'
    });
    var marker = new AMap.Marker({
      position: [-lat, lng]
    });
    map.add(marker);
  }
};
var url =
  'https://webapi.amap.com/maps?v=1.4.15&key=aec547693c2c2a7424add1187ba5e951&callback=onLoad';
var jsapi = document.createElement('script');
jsapi.src = url;
document.head.appendChild(jsapi);
