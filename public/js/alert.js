// type is 'success' or 'error'
const hideAlert = () => {
  const el = document.querySelector('.alert');
  if (el) el.parentElement.removeChild(el);
};
const showAlert = (type, msg, time = 3) => {
  //新alert出现之前先除去已存在的
  hideAlert();
  const markup = `<div class="alert alert--${type}">${msg}</div>`;
  //insertAdjacentHTML(swhere,stext) -在指定的地方插入html标签语句
  document.querySelector('body').insertAdjacentHTML('beforebegin', markup);
  window.setTimeout(hideAlert, time * 1000);
};

const hideConfirm = () => {
  // console.log('取消');
  const el = document.getElementById('showConfirm');
  if (el) el.parentElement.removeChild(el);
};
const deleteMyAccount = async (req, res) => {
  try {
    const res = await axios({
      method: 'DELETE',
      url: 'http://localhost:3000/api/v1/users/deleteMe'
    });
    if (res.status === 204) {
      // console.log('确认')
      hideConfirm();
      showAlert('success', `您已成功注销`);
      window.setTimeout(() => {
        location.reload(true);
      }, 1000);
    }
  } catch (err) {
    showAlert('error',`${err.response.data.message}`);
  }
};
const showConfirm = msg => {
  hideConfirm();
  // console.log('注销用户');
  const markup = `<div id='showConfirm' class="alert alert--confirm">
    <p>${msg}</p>
    </br>
    <button id='btnConfirm' class='btn-small' onclick=deleteMyAccount()>确认</button>
    &nbsp;&nbsp;&nbsp
    <button id='btnCanceled' class='btn-small' onclick=hideConfirm()>取消</button>
  </div>`;
  document.querySelector('body').insertAdjacentHTML('beforebegin', markup);
};
