const loginForm = document.getElementById('loginForm')
const signupForm = document.getElementById('signupForm')
const logoutBtn = document.getElementById('logoutBtn')

const signup = async (email,name,password,passwordConfirm)=>{
   try {
    const res = await axios({
      method: 'POST',
      url: 'http://localhost:3000/api/v1/users/signup',
      data: {
        email,
        name,
        password,
        passwordConfirm,
      }
    });
    if (res.data.status === 'success') {
      showAlert('success', '注册成功');
      window.setTimeout(()=>{
        location.assign('/');
      },1000)
    }
  } catch (err) {
    showAlert('error', err.response.data.message);
  }
}
const login = async (email, password) => {
  try {
    const res = await axios({
      method: 'POST',
      url: 'http://localhost:3000/api/v1/users/login',
      data: {
        email,
        password
      }
    });
    if (res.data.status === 'success') {
      showAlert('success', '登陆成功');
      window.setTimeout(()=>{
        location.assign('/');
      },1000)
    }
  } catch (err) {
    showAlert('error', err.response.data.message);
  }
};
const logout = async () => {
  try {
    const res = await axios({
      method: 'GET',
      url: 'http://localhost:3000/api/v1/users/logout'
    });
    if (res.data.status === 'success') {
      //!! reload from server,not browser cache
      console.log(res.data);
      location.reload(true);
    }
  } catch (err) {
    console.log(err.response);
    showAlert('error', '似乎出了点问题...');
  }
};

if (loginForm) {
  loginForm.addEventListener('submit', event => {
    //表单默认提交刷新页面
    event.preventDefault();
    const email = document.getElementById('email').value;
    const password = document.getElementById('password').value;
    login(email, password);
  });
}

if (logoutBtn) {
  logoutBtn.addEventListener('click',logout);
}

if(signupForm){
  signupForm.addEventListener('submit', event => {
    event.preventDefault();
    const name = document.getElementById('name').value;
    const email = document.getElementById('email').value;
    const password = document.getElementById('password').value;
    const passwordConfirm = document.getElementById('password').value;
    signup(email,name,password,passwordConfirm);
  });
}
