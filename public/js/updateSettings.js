const userDataForm = document.querySelector('.form-user-data');
const userPasswordForm = document.querySelector('.form-user-password');
const setMeInactive = document.getElementById('setMeInactive');
const updateSettings = async (data, type) => {
  try {
    const res = await axios({
      method: 'PATCH',
      url:
        type === '信息'
          ? 'http://localhost:3000/api/v1/users/updateMe'
          : 'http://localhost:3000/api/v1/users/updateMyPassword',
      data
    });
    if (res.data.status === 'success') {
      showAlert('success', `${type}更新成功`);
      window.setTimeout(() => {
        location.reload(true);
      }, 1000);
    }
  } catch (err) {
    showAlert('error', err.response.data.message);
  }
};

if (userDataForm) {
  userDataForm.addEventListener('submit', event => {
    event.preventDefault();
    const name = document.getElementById('name').value;
    const email = document.getElementById('email').value;
    updateSettings({ email, name }, '信息');
  });
}
if (userPasswordForm) {
  userPasswordForm.addEventListener('submit', async event => {
    event.preventDefault();
    document.querySelector('.btn--save-password').innerHTML = '正在保存...';
    const passwordCurrent = document.getElementById('password-current').value;
    const password = document.getElementById('password').value;
    const passwordConfirm = document.getElementById('password-confirm').value;
    try {
      //更新密码完毕后，将input字段置空
      await updateSettings(
        { passwordCurrent, password, passwordConfirm },
        '密码'
      );
      document.querySelector('.btn--save-password').innerHTML = '保存密码';
      passwordCurrent.value = '';
      password.value = '';
      passwordConfirm.value = '';
    } catch (err) {
      showAlert('error', '出错了...');
    }
  });
}
if (setMeInactive) {
  setMeInactive.addEventListener('click', () => {
    showConfirm('您确定要注销账号吗？\n 此操作不可恢复');
  });
}
