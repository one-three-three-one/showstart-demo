/* eslint-disable */
const bookBtn = document.getElementById('book-show');

const bookShow = async () => {
  try {
    const res = await axios({
      method: 'GET',
      url: 'http://localhost:3000/api/v1/bookings'
    });
    // console.log(res);
    location.assign(res.data.code);
  } catch (err) {
    console.log(err);
    showAlert('error', err);
  }
};
if (bookBtn) {
  bookBtn.addEventListener('click', e => {
    e.target.textContent = '请等待跳转...';
    bookShow();
  });
}
